﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class PovrsinskiObjekatMapiranja :SubclassMap<PovrsinskiObjekat>
    {
        public PovrsinskiObjekatMapiranja()
        {
            Table("POVRSINSKI_OBJEKAT");

            KeyColumn("ID_GOBJ");

            Map(x => x.VODENE_POVRSINE_FLEG).Column("VODENE_POVRSINE_FLEG");
            Map(x => x.TIP_VODENIH).Column("TIP_VODENIH");
            Map(x => x.UZVISENJE_FLEG).Column("UZVISENJE_FLEG");
            Map(x => x.NADMORSKA_VISINA).Column("NADMORSKA_VISINA");

      //      HasMany(x => x.ListaGeografskihObjekata).KeyColumn("ID_UZVISENJA").LazyLoad().Cascade.All().Inverse();
      //      HasMany(x => x.ListaVrhova).KeyColumn("ID_UZVISENJA").LazyLoad().Cascade.All().Inverse();
     //       HasMany(x => x.ListaLinijskih).KeyColumn("ID_POV_GOBJ").LazyLoad().Cascade.All().Inverse();
        }
    }
}
