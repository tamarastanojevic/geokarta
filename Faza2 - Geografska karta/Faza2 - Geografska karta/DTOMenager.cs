﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;

namespace Faza2___Geografska_karta
{
    public class DTOMenager
    {
        public static void DodajNovi(GeografskiObjekat go)
        {
            try
            {
                ISession s = DataLayer.GetSession();

/*                if(go is TackastiObjekat)
                {
                    TackastiObjekat t = (TackastiObjekat)go;
                    t.ID_UZVISENJA = null;
                    t.Nadmorska_visina = 0;
                    s.SaveOrUpdate(t);
                }
  */              
                s.SaveOrUpdate(go);
                s.Flush();

                s.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public static List<GeografskiObjekat> CitajSve()
        {

            List<GeografskiObjekat> lgeo = new List<GeografskiObjekat>();
            try
            {
                ISession s = DataLayer.GetSession();


                IEnumerable<GeografskiObjekat> lista = from x in s.Query<GeografskiObjekat>() select x;
                //IList<GeografskiObjekat> ll = s.QueryOver<GeografskiObjekat>().List<GeografskiObjekat>();

                //  lgeo = ll.ToList();
                lgeo = lista.ToList();

                s.Close();
            }
            catch (Exception ex)
            {

            }
            return lgeo;
        }

        public static Simbol VratiSimbol(int broj)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IList<Simbol> simboli = s.QueryOver<Simbol>()
                       .List<Simbol>();

                foreach(Simbol sim in simboli)
                {
                    if(broj>=sim.OD_BROJ_STANOVNIKA && broj<=sim.DO_BROJ_STANOVNIKA)
                    {
                        s.Close();
                        return sim;
                    }
                }

                s.Close();
                return null;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        public static void Brisi(GeografskiObjekat go)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                if(go.TIP=="L")
                {
                    LinijskiObjekat lo;

                    IList<LinijskiObjekat> ll = s.QueryOver<LinijskiObjekat>()
                        .Where(x => x.ID_GOBJ == go.ID_GOBJ).List<LinijskiObjekat>();

                    lo = (LinijskiObjekat)ll[0];

                    lo.ID_UZVISENJA = null;
                    lo.Nadmorska_visina = 0;

                    s.Delete(lo);
                    s.Flush();
                }
                else if(go.TIP=="T")
                {
                    TackastiObjekat to;

                    IList<TackastiObjekat> tl = s.QueryOver<TackastiObjekat>()
                        .Where(x => x.ID_GOBJ == go.ID_GOBJ).List<TackastiObjekat>();

                    to = (TackastiObjekat)tl[0];

                    to.ID_UZVISENJA = null;
                    to.Nadmorska_visina = 0;

                    s.Delete(to);
                    s.Flush();
                }
                else if(go.TIP=="P")
                {
                    PovrsinskiObjekat po;

                    IList<PovrsinskiObjekat> pl = s.QueryOver<PovrsinskiObjekat>()
                       .Where(x => x.ID_GOBJ == go.ID_GOBJ).List<PovrsinskiObjekat>();

                    po = (PovrsinskiObjekat)pl[0];

                    foreach (TackastiObjekat t in po.ListaGeografskihObjekata)
                    {
                        t.ID_UZVISENJA = null;
                    }

                    po.ListaGeografskihObjekata = null;
                    po.ID_UZVISENJA = null;
                    po.Nadmorska_visina = 0;

                    s.Delete(po);
                    s.Flush();

                }

                s.Close();

            }
            catch(Exception ex)
            {
                ex.ToString();
            }
        }

        public static void Izmeni(GeografskiObjekat go)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(go);
                s.Flush();

                s.Close();
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
        }

        public static List<TackastiObjekat> CitajTackaste()
        {
            List<TackastiObjekat> lt = new List<TackastiObjekat>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<TackastiObjekat> et = from x in s.Query<TackastiObjekat>() select x;

                lt = et.ToList();

                s.Close();
            }
            catch (Exception ex)
            {

            }

            return lt;
        }

        public static List<PovrsinskiObjekat> CitajPovrsinske()
        {
            List<PovrsinskiObjekat> lp = new List<PovrsinskiObjekat>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<PovrsinskiObjekat> ep = from x in s.Query<PovrsinskiObjekat>() select x;

                lp = ep.ToList();

                s.Close();
            }
            catch (Exception ex)
            {

            }

            return lp;
        }

        public static List<LinijskiObjekat> CitajLinijske()
        {
            List<LinijskiObjekat> ll = new List<LinijskiObjekat>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<LinijskiObjekat> el = from x in s.Query<LinijskiObjekat>() select x;

                ll = el.ToList();

                s.Close();
            }
            catch (Exception ex)
            {

            }

            return ll;
        }

        public static void DodajSadrziTackastom(Sadrzi sad, int x)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat to = s.Load<TackastiObjekat>(x);

                to.ListaLinijskih.Add(sad);

                s.SaveOrUpdate(to);
                s.Close();
            }
            catch(Exception ex)
            {

            }
        }

        public static void DodajSadrziLinijskom(Sadrzi sad, int x)
        {
            LinijskiObjekat lobj = new LinijskiObjekat();
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo = s.Load<LinijskiObjekat>(x);

                lo.ListaTackastih.Add(sad);

                s.SaveOrUpdate(lo);

                s.Close();
            }
            catch (Exception ex)
            {

            }
        }

        public static void DodajSastojiPovrsinskom(SastojiSeOd ssd, int x)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PovrsinskiObjekat po = s.Load<PovrsinskiObjekat>(x);

                po.ListaLinijskih.Add(ssd);

                s.SaveOrUpdate(po);
                s.Close();
            }
            catch(Exception ex)
            {

            }
        }

        public static void DodajSastojiLinijskom(SastojiSeOd ssd, int x)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo = s.Load<LinijskiObjekat>(x);

                lo.ListaPovrsinskih.Add(ssd);

                s.SaveOrUpdate(lo);
                s.Close();
            }
            catch (Exception ex)
            {

            }
        }

        public static List<PovrsinskiObjekat> CitajUzvisenja()
        {
            List<PovrsinskiObjekat> lpo = new List<PovrsinskiObjekat>();

            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<PovrsinskiObjekat> epo = from x in s.Query<PovrsinskiObjekat>()
                                                     where x.UZVISENJE_FLEG == true
                                                     select x;
                lpo = epo.ToList();

                s.Close();
            }
            catch(Exception ex)
            {

            }

            return lpo;
        }

    }
}
