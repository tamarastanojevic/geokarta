﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class Simbol
    {
        public virtual int ID_SIMBOL { get; set; }
        public virtual int OD_BROJ_STANOVNIKA { get; set; }
        public virtual int DO_BROJ_STANOVNIKA { get; set; }
        public virtual string NAZIV { get; set; }

        public virtual IList<TackastiObjekat> ListaNaseljenihMesta { get; set; }

        public Simbol()
        {
            ListaNaseljenihMesta = new List<TackastiObjekat>();
        }
    }
}
