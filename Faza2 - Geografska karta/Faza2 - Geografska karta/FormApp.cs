﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class FormApp : Form
    {
        PovrsinskiObjekat visinski;
        int visinaP;

        public FormApp()
        {
            InitializeComponent();
        }

        private void btnJos_Click(object sender, EventArgs e)
        {
            this.Width = 823;
            btnJos.Visible = false;

            List<GeografskiObjekat> lgeo = DTOMenager.CitajSve();

            dgv1.Rows.Clear();

            foreach(GeografskiObjekat go in lgeo)
            {
                dgv1.Rows.Add(go.ID_GOBJ, go.Naziv, go.TIP, go);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Width = 574;
            btnJos.Visible = true;
        }

        private void rbtnLin_CheckedChanged(object sender, EventArgs e)
        {
            linijski1.Visible = true;
            tackasti1.Visible = false;
            povrsinski1.Visible = false;
        }

        private void rbtnTac_CheckedChanged(object sender, EventArgs e)
        {
            linijski1.Visible = false;
            tackasti1.Visible = true;
            povrsinski1.Visible = false;
        }

        private void rbtnPov_CheckedChanged(object sender, EventArgs e)
        {
            linijski1.Visible = false;
            tackasti1.Visible = false;
            povrsinski1.Visible = true;

            
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (rbtnLin.Checked)
            {
                linijski1.lo.Naziv = tbxNaziv.Text;
                linijski1.lo.ID_UZVISENJA = visinski;
                linijski1.lo.Nadmorska_visina = visinaP;
                /*    String lista = "";
                    foreach(KoordinateLinijski kl in linijski1.lo.ListaKoordinata)
                    {
                        lista+=kl.X+" "+kl.Y + "\n";
                    }
                    MessageBox.Show(linijski1.lo.Naziv+"\n"+linijski1.lo.DUZINA+"\n"+lista);*/

                DTOMenager.DodajNovi(linijski1.lo);

                MessageBox.Show("Dodat je novi linijski");

                linijski1.PopuniListuP();
                linijski1.PopuniListuT();
                linijski1.lo = new LinijskiObjekat();
                linijski1.lo.REKA_FLEG = true;
               


                tbxNaziv.ResetText();
                linijski1.CititKontorle();
            }
            else if (rbtnPov.Checked)
            {
                povrsinski1.po.Naziv = tbxNaziv.Text;
                povrsinski1.po.ID_UZVISENJA = visinski;
                povrsinski1.po.Nadmorska_visina = visinaP;
                /*  String lista = "";
                  foreach(Vrh v in povrsinski1.po.ListaVrhova)
                  {
                      lista+=v.IME+" "+v.VISINA + "\n";
                  }
                  MessageBox.Show(povrsinski1.po.Naziv + "\n" + lista);*/

                DTOMenager.DodajNovi(povrsinski1.po);

                MessageBox.Show("Dodat je novi povrinski");

                povrsinski1.po = new PovrsinskiObjekat();
                povrsinski1.po.VODENE_POVRSINE_FLEG = true;

                tbxNaziv.ResetText();
                povrsinski1.CistiKontorle();
            }
            else if (rbtnTac.Checked)
            {
                tackasti1.to.Naziv = tbxNaziv.Text;
                tackasti1.to.ID_UZVISENJA = visinski;
                tackasti1.to.Nadmorska_visina = visinaP;
                //               MessageBox.Show(tackasti1.to.Naziv + "\n" + tackasti1.to.KOORD_X + "\n" + tackasti1.to.KOORD_Y + "\n" + tackasti1.to.NASELJENO_FLEG + "\n" + tackasti1.to.TURISTICKO_FLEG);

                Simbol simbol = DTOMenager.VratiSimbol(tackasti1.to.BROJ_STANOVNIKA);
                tackasti1.to.ID_SIMBOLA = simbol;

                DTOMenager.DodajNovi(tackasti1.to);

                MessageBox.Show("Dodat je novi tackasti");

                tackasti1.to = new TackastiObjekat();

                tackasti1.CistiKontrole();
                tbxNaziv.ResetText();
            }

            popuniListuPov();
        }

   
        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            GeografskiObjekat gobj =(GeografskiObjekat) dgv1.SelectedRows[0].Cells[3].Value;

            FormEditDelete fed = new FormEditDelete(gobj);
            var rez = fed.ShowDialog();

            if(rez==DialogResult.OK)
            {
                List<GeografskiObjekat> lgeo = DTOMenager.CitajSve();

                dgv1.Rows.Clear();

                foreach (GeografskiObjekat go in lgeo)
                {
                    dgv1.Rows.Add(go.ID_GOBJ, go.Naziv, go.TIP, go);
                }
            }

        }

        private void popuniListuPov()
        {
            List<PovrsinskiObjekat> po = new List<PovrsinskiObjekat>();

            po = DTOMenager.CitajUzvisenja();

            dtgPov.Rows.Clear();

            foreach(PovrsinskiObjekat p in po)
            {
                dtgPov.Rows.Add(p.Naziv, false, p);
            }
        }

        private void FormApp_Load(object sender, EventArgs e)
        {
            popuniListuPov();
        }

        private void dtgPov_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cbc = new DataGridViewCheckBoxCell();
            cbc = (DataGridViewCheckBoxCell)dtgPov.Rows[dtgPov.CurrentRow.Index].Cells[1];
            if ((Boolean)cbc.Value == false)
            {

                visinski = null;
                visinaP = 0;

                for(int i=0; i<dtgPov.Rows.Count; i++)
                {
                    dtgPov.Rows[i].Cells[1].Value = false;
                }

                cbc.Value = true;

                FormVisina fv = new FormVisina();
                var rez = fv.ShowDialog();
                
                if (rez == DialogResult.OK)
                {
                    visinski = (PovrsinskiObjekat)dtgPov.Rows[dtgPov.CurrentRow.Index].Cells[2].Value;
                    visinaP = fv.nadVisina;
                }

                
                
            }
            else
            {
                visinski = null;
                visinaP = 0;
            }
        }
    }
}
