﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class Znamenitost
    {
        public virtual int ID_ZNAMENITOST { get; set; }
        public virtual string NAZIV { get; set; }
        public virtual TackastiObjekat ID_NASELJENO { get; set; }

        public Znamenitost()
        {
        }
    }
}
