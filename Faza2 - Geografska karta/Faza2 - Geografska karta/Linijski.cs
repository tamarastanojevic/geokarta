﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class Linijski : UserControl
    {
       public LinijskiObjekat lo;

        public Linijski()
        {
            InitializeComponent();
            lo = new LinijskiObjekat();
            lo.REKA_FLEG = true;
            PopuniListuT();
            PopuniListuP();
        }

        private void rbtnReka_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnReka.Checked)
            {
                lblKlasa.Visible = false;
                cmbKlase.Visible = false;

                lblImeD.Visible = false;
                tbxImeD.Visible = false;


                lo.REKA_FLEG = true;
                lo.PUT_FLEG = false;
                lo.GRANICNA_LINIJA_FLEG = false;
            }
        }

        private void rbtnPut_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnPut.Checked)
            {
                lblKlasa.Visible = true;
                cmbKlase.Visible = true;

                lblImeD.Visible = false;
                tbxImeD.Visible = false;

                lo.REKA_FLEG = false;
                lo.PUT_FLEG = true;
                lo.GRANICNA_LINIJA_FLEG = false;
            }
        }

        private void rbtnGran_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnGran.Checked)
            {
                lblKlasa.Visible = false;
                cmbKlase.Visible = false;

                lblImeD.Visible = true;
                tbxImeD.Visible = true;

                lo.REKA_FLEG = false;
                lo.PUT_FLEG = false;
                lo.GRANICNA_LINIJA_FLEG = true;
            }
        }

        private void btnDodajKoo_Click(object sender, EventArgs e)
        {
            if (nupX.Value > 0 && nupY.Value > 0)
            {
                KoordinateLinijski ko = new KoordinateLinijski();
                ko.X = (float)nupX.Value;
                ko.Y = (float)nupY.Value;
                ko.ID_GOBJ = lo;

                lo.ListaKoordinata.Add(ko);

                listBox1.Items.Clear();
                foreach(KoordinateLinijski k in lo.ListaKoordinata)
                {
                    String s ="X : "+ k.X + " Y : " + k.Y;
                    listBox1.Items.Add(s);
                }
            }
        }

        private void numericUpDown1_Leave(object sender, EventArgs e)
        {
            lo.DUZINA = (int)numericUpDown1.Value;
        }

        private void tbxImeD_Leave(object sender, EventArgs e)
        {
            lo.IME_DRZAVE = tbxImeD.Text;
        }

        private void cmbKlase_Leave(object sender, EventArgs e)
        {
            lo.KLASA_PUTA = cmbKlase.SelectedItem.ToString();
        }

        public void CititKontorle()
        {
            numericUpDown1.ResetText();
            rbtnReka.Checked = true;
            rbtnPut.Checked = true;
            rbtnGran.Checked = true;
            tbxImeD.ResetText();
            nupX.ResetText();
            nupY.ResetText();
            listBox1.Items.Clear();
        }
      
        
        public void PopuniListuT()
        {
            List<TackastiObjekat> lto = new List<TackastiObjekat>();

            lto = DTOMenager.CitajTackaste();

            dataGridView1.Rows.Clear();

            foreach(TackastiObjekat t in lto)
            {
                dataGridView1.Rows.Add(t.Naziv, false, t);
            }
        }

        public void PopuniListuP()
        {
            List<PovrsinskiObjekat> lpo = new List<PovrsinskiObjekat>();

            lpo = DTOMenager.CitajPovrsinske();

            dataGridView2.Rows.Clear();

            foreach(PovrsinskiObjekat p in lpo)
            {
                dataGridView2.Rows.Add(p.Naziv, false, p);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cbc = new DataGridViewCheckBoxCell();
            cbc = (DataGridViewCheckBoxCell) dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1];
            if ((Boolean)cbc.Value==false)
            {
                cbc.Value = true;

                Sadrzi novoS = new Sadrzi();

                novoS.ID_LIN_GOBJ = lo;
                TackastiObjekat to;
                to = (TackastiObjekat)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value;


                DTOMenager.DodajSadrziTackastom(novoS, to.ID_GOBJ);

                novoS.ID_TAC_GOBJ = to;


                lo.ListaTackastih.Add(novoS);

                FormSadrziL fsl = new FormSadrziL();
                var rez = fsl.ShowDialog();

                if (rez == DialogResult.OK)
                {
                    novoS.REDNI_BROJ = fsl.rbr;
                    novoS.RASTOJANJE_DO_SLEDECEG = fsl.rastojanjeSl;
                    novoS.RASTOJANJE_DO_PROSLOG = fsl.rastojanjePro;
                }
            }
            else
            {
                cbc.Value = false;

            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cbc = new DataGridViewCheckBoxCell();
            cbc = (DataGridViewCheckBoxCell)dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[1];
            if ((Boolean)cbc.Value == false)
            {
                cbc.Value = true;

                SastojiSeOd ssd = new SastojiSeOd();

                ssd.ID_LIN_GOBJ = lo;

                PovrsinskiObjekat po;

                po = (PovrsinskiObjekat)dataGridView2.Rows[dataGridView2.CurrentRow.Index].Cells[2].Value;

                DTOMenager.DodajSastojiPovrsinskom(ssd, po.ID_GOBJ);
                ssd.ID_POV_GOBJ = po;

                lo.ListaPovrsinskih.Add(ssd);

            }
            else
            {
                cbc.Value = false;

            }
        }
    }
}
