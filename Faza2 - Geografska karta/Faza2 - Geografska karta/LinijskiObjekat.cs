﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class LinijskiObjekat : GeografskiObjekat
    {
        public virtual int DUZINA { get; set; }
        public virtual bool REKA_FLEG { get; set; }
        public virtual bool PUT_FLEG { get; set; }
        public virtual string KLASA_PUTA { get; set; }
        public virtual bool GRANICNA_LINIJA_FLEG { get; set; }
        public virtual string IME_DRZAVE { get; set; }

        public virtual IList<SastojiSeOd> ListaPovrsinskih { get; set; }
        public virtual IList<Sadrzi> ListaTackastih { get; set; }
        public virtual IList<KoordinateLinijski> ListaKoordinata { get; set; }

        public LinijskiObjekat()
        {
            this.TIP = "L";

            this.GRANICNA_LINIJA_FLEG = false;
            this.PUT_FLEG = false;
            this.REKA_FLEG = false;

            ListaPovrsinskih = new List<SastojiSeOd>();
            ListaTackastih = new List<Sadrzi>();
            ListaKoordinata = new List<KoordinateLinijski>();
        }
    }
}
