﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class Povrsinski : UserControl
    {

        public PovrsinskiObjekat po;

        public Povrsinski()
        {
            InitializeComponent();
            po = new PovrsinskiObjekat();
            po.VODENE_POVRSINE_FLEG = true;
            PopuniListuL();
        }

        private void rbtUzvisenje_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtUzvisenje.Checked)
            {
                po.UZVISENJE_FLEG = true;
                po.VODENE_POVRSINE_FLEG = false;

                lblNadV.Visible = true;
                lblVisnaV.Visible = true;
                lblVrhI.Visible = true;
                nupNadV.Visible = true;
                nupVisinaV.Visible = true;
                tbxImeV.Visible = true;
                lblTipV.Visible = false;
                cbxTipV.Visible = false;
                btnDodajVrh.Visible = true;
                listBox1.Visible = true;
            }

        }

        private void rbtVod_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtVod.Checked)
            {
                po.VODENE_POVRSINE_FLEG = true;
                po.UZVISENJE_FLEG = false;

                lblNadV.Visible = false;
                lblVisnaV.Visible = false;
                lblVrhI.Visible = false;
                nupNadV.Visible = false;
                nupVisinaV.Visible = false;
                tbxImeV.Visible = false;
                lblTipV.Visible = true;
                cbxTipV.Visible = true;
                btnDodajVrh.Visible = false;
                listBox1.Visible = false;
            }
        }

        private void nupNadV_Leave(object sender, EventArgs e)
        {
            po.NADMORSKA_VISINA = (int)nupNadV.Value;
        }

        private void cbxTipV_Leave(object sender, EventArgs e)
        {
            if (cbxTipV.SelectedItem != null)
            {
                po.TIP_VODENIH = cbxTipV.SelectedItem.ToString();
            }
        }


        private void btnDodajVrh_Click(object sender, EventArgs e)
        {
            Vrh v = new Vrh();

            v.IME = tbxImeV.Text;
            v.VISINA = (int)nupVisinaV.Value;
            v.ID_UZVISENJA = po;

            po.ListaVrhova.Add(v);

            listBox1.Items.Clear();
            foreach (Vrh vr in po.ListaVrhova)
            {
                String s = vr.IME + " " + vr.VISINA;
                listBox1.Items.Add(s);
            }

        }

        public void CistiKontorle()
        {
            listBox1.Items.Clear();
            nupVisinaV.ResetText();
            nupNadV.ResetText();
            tbxImeV.ResetText();
            cbxTipV.ResetText();
            rbtVod.Checked = true;
            rbtUzvisenje.Checked = false;
        }

        public void PopuniListuL()
        {
            List<LinijskiObjekat> ll = new List<LinijskiObjekat>();

            ll = DTOMenager.CitajLinijske();

            foreach(LinijskiObjekat l in ll)
            {
                dataGridView1.Rows.Add(l.Naziv, false, l);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cbc = new DataGridViewCheckBoxCell();
            cbc = (DataGridViewCheckBoxCell)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1];
            if ((Boolean)cbc.Value == false)
            {
                cbc.Value = true;

                SastojiSeOd ssd = new SastojiSeOd();

                ssd.ID_POV_GOBJ = po;

                LinijskiObjekat lo;
                lo = (LinijskiObjekat)dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value;

                DTOMenager.DodajSastojiLinijskom(ssd, lo.ID_GOBJ);
                ssd.ID_LIN_GOBJ = lo;

                po.ListaLinijskih.Add(ssd);

            }
            else
            {
                cbc.Value = false;

            }
        }

        private void Povrsinski_Load(object sender, EventArgs e)
        {

        }
    }
}
