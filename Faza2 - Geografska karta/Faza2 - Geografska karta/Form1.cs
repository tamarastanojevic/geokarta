﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        #region CREATE
        private void btnDodajL_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo = new LinijskiObjekat();

                lo.Naziv = "E-75";
                lo.TIP = "T";
                lo.DUZINA = 160800;
                lo.REKA_FLEG = false;
                lo.PUT_FLEG = true;
                lo.KLASA_PUTA = "1.";
                lo.GRANICNA_LINIJA_FLEG = false;

                KoordinateLinijski kl = new KoordinateLinijski();
                kl.ID_GOBJ = lo;
                kl.X = 28;
                kl.Y = 29;

                lo.ListaKoordinata.Add(kl);

                TackastiObjekat to;
                IList<TackastiObjekat> listatac = s.QueryOver<TackastiObjekat>().Where(x => x.Naziv == "Nis").List<TackastiObjekat>();
                to = listatac[0];

                Sadrzi sd = new Sadrzi();
                sd.ID_LIN_GOBJ = lo;
                sd.ID_TAC_GOBJ = to;
                sd.REDNI_BROJ = 2;
                sd.RASTOJANJE_DO_PROSLOG = 220;
                sd.RASTOJANJE_DO_SLEDECEG = 442;

                lo.ListaTackastih.Add(sd);

                PovrsinskiObjekat po;
                IList<PovrsinskiObjekat> listapov = s.QueryOver<PovrsinskiObjekat>().Where(x => x.Naziv == "Bovansko jezero").List<PovrsinskiObjekat>();
                po = listapov[0];

                SastojiSeOd sod = new SastojiSeOd();
                sod.ID_LIN_GOBJ = lo;
                sod.ID_POV_GOBJ = po;

                lo.ListaPovrsinskih.Add(sod);

                s.SaveOrUpdate(lo);
                s.Flush();

                MessageBox.Show("Uspesno je dodat novi linijski objekat: " + lo.Naziv + " uz koordinate x=" + kl.X + ",y=" + kl.Y + " i tackasti objekat " + to.Naziv);

                s.Close();
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnDodajT_Click(object sender, EventArgs e)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat to = new TackastiObjekat();

                Simbol simbol;
                IList<Simbol> listasim = s.QueryOver<Simbol>().Where(x => x.NAZIV == "Gradic").List<Simbol>();
                simbol = listasim[0];

                to.Naziv = "Kursumlija";
                to.KOORD_X = 45;
                to.KOORD_Y = 40;
                to.DATUM_EVIDENCIJE = DateTime.Now;
                to.NASELJENO_FLEG = true;
                to.OPSTINA = "Kursumlija";
                to.BROJ_STANOVNIKA = 25000;
                to.DATUM_OSNIVANJA = DateTime.Now;
                to.TURISTICKO_FLEG = false;
                to.ID_SIMBOLA = simbol;

            
                LinijskiObjekat lo;
                IList<LinijskiObjekat> listal = s.QueryOver<LinijskiObjekat>().Where(x => x.Naziv == "E15").List<LinijskiObjekat>();
                lo = listal[0];

                Sadrzi sd = new Sadrzi();
                sd.ID_LIN_GOBJ = lo;
                sd.ID_TAC_GOBJ = to;
                sd.REDNI_BROJ = 10;
                sd.RASTOJANJE_DO_PROSLOG = 45;
                sd.RASTOJANJE_DO_SLEDECEG = 55;

                to.ListaLinijskih.Add(sd);

                s.SaveOrUpdate(to);
                s.Flush();

                MessageBox.Show("Uspesno je dodat novi tackasti objekat: " + to.Naziv+" sa koordinatama x="+to.KOORD_X+",y="+to.KOORD_Y+",\n koji se nalazi na linijskom objektu: "+lo.Naziv+" pod rednim brojem "+sd.REDNI_BROJ);
                
                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnDodajP_Click(object sender, EventArgs e)
        {
            
            try
            {
                ISession s = DataLayer.GetSession();

                PovrsinskiObjekat po = new PovrsinskiObjekat();

                TackastiObjekat tt;

                IList<TackastiObjekat> tl = s.QueryOver<TackastiObjekat>()
                    .Where(x => x.Naziv == "Nis").List<TackastiObjekat>();

                tt = (TackastiObjekat)tl[0];
                tt.ID_UZVISENJA = po;

                LinijskiObjekat ll;

                IList<LinijskiObjekat> listalin = s.QueryOver<LinijskiObjekat>()
                    .Where(x => x.Naziv == "E15").List<LinijskiObjekat>();

                ll = (LinijskiObjekat)listalin[0];

                SastojiSeOd sas = new SastojiSeOd();
                sas.ID_LIN_GOBJ = ll;
                sas.ID_POV_GOBJ = po;

                Vrh v = new Vrh();
                v.IME = "Bubanj Vrh";
                v.ID_UZVISENJA = po;
                v.VISINA = 1420;

                po.Naziv = "Bubanj";
                po.TIP = "P";
                po.ListaGeografskihObjekata.Add(tt);
                po.ListaLinijskih.Add(sas);
                po.ListaVrhova.Add(v);
                po.NADMORSKA_VISINA = 1400;
                po.UZVISENJE_FLEG = true;
                po.VODENE_POVRSINE_FLEG = false;
                
                s.SaveOrUpdate(po);
                s.Flush();

                MessageBox.Show("Uspesno je dodat novi povrsinski objekat: " + po.Naziv+ "\nuz njega i vrh: "+v.IME+", za grad: "+tt.Naziv+" i linijski obj: "+ll.Naziv);
                
                s.Close();
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }
        #endregion

        #region READ
        private void btnCitajL_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat l;
                IList<LinijskiObjekat> listal = s.QueryOver<LinijskiObjekat>().Where(x => x.Naziv == "E-75").List<LinijskiObjekat>();
                l = listal[0];

                MessageBox.Show("Podaci o linijskom objektu: \nid: " + l.ID_GOBJ + ", naziv: " + l.Naziv + ", tip: " + l.TIP + ", duzina: " + l.DUZINA + ", reka fleg: " + l.REKA_FLEG + ", put fleg: " + l.PUT_FLEG);

                String poruka = "";

                foreach (SastojiSeOd to in l.ListaPovrsinskih)
                {
                    poruka = poruka + "id linijskog: " + to.ID_LIN_GOBJ.Naziv + ", naziv: " + to.ID_LIN_GOBJ.Naziv + ", naziv povrsinskog: " + to.ID_POV_GOBJ.Naziv+"\n";
                }
                MessageBox.Show("Ovaj linijski objekat se sastoji od sledecih linijskih objekata:\n" + poruka);

                poruka = "";
                foreach (KoordinateLinijski ko in l.ListaKoordinata)
                {
                    poruka = poruka + "X: " + ko.X + "Y: " + ko.Y + "\n";
                }
                MessageBox.Show("Ovaj linijski objekat ima koordinate :\n" + poruka);
                
                poruka = "";
                foreach (Sadrzi sd in l.ListaTackastih)
                {
                    poruka = poruka + sd.ID_TAC_GOBJ.Naziv + "\n";
                }
                MessageBox.Show("Ovaj linijski objekat sadrzi sledece tackaste objekte: \n" + poruka);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnCitajT_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat t;
                IList<TackastiObjekat> listatac = s.QueryOver<TackastiObjekat>().Where(x=>x.Naziv== "Presevo").List<TackastiObjekat>();
                t = listatac[0];

                MessageBox.Show("Podaci o tackastom objektu:\nid: " + t.ID_GOBJ + ", naziv: " + t.Naziv + ", tip: " + t.TIP + ", koordX: " + t.KOORD_X + ", koordY: " + t.KOORD_Y + ", \ndatum evidencije: " + t.DATUM_EVIDENCIJE.ToString() + ", naseljeno mesto fleg: " + t.NASELJENO_FLEG + ", \n opstina: " + t.OPSTINA + ", broj stanovnika: " + t.BROJ_STANOVNIKA + ", \ndaum osnivanja: " + t.DATUM_OSNIVANJA + ", turisticko fleg: " + t.TURISTICKO_FLEG + ", datum tur: " + t.DATUM_TUR + ", tip: " + t.TIP_TURISTICKOG);
                
                MessageBox.Show("Ovaj tackasti objekat ima simbol " + t.ID_SIMBOLA.NAZIV);
                
                String poruka = "";

                foreach (Znamenitost z in t.ListaZnamenitosti)
                {
                    poruka = poruka + z.NAZIV + "\n";
                }
                MessageBox.Show("Sve znamenitosti ovog tackastog objekata su: \n" + poruka);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnCitajP_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PovrsinskiObjekat p;
                IList<PovrsinskiObjekat> listapov = s.QueryOver<PovrsinskiObjekat>().Where(x=>x.Naziv== "Bubanj").List<PovrsinskiObjekat>();
                p = listapov[0];

                MessageBox.Show("Podaci o provrsinskom objektu: \nid: " + p.ID_GOBJ + ", naziv: " + p.Naziv + ", tip: " + p.TIP + ", \nvodene povrsine fleg: " + p.VODENE_POVRSINE_FLEG + ", tip vodenih: " + p.TIP_VODENIH + ", uzvisenje fleg: " + p.UZVISENJE_FLEG + ", nadmorska visina: " + p.NADMORSKA_VISINA);


                String poruka = "";

                foreach (Vrh to in p.ListaVrhova)
                {
                    poruka = poruka + to.IME + " " + to.VISINA + "\n";
                }
                MessageBox.Show("Na njemu se nalaze sledeca uzvisenja: \n" + poruka);

                poruka = "";
                foreach (GeografskiObjekat go in p.ListaGeografskihObjekata)
                {
                    poruka = poruka + go.ID_GOBJ + " " + go.Naziv + "\n";
                }
                MessageBox.Show("Na njemu se nalaze sledeci geografski objekti: \n" + poruka);

                poruka = "";
                foreach (SastojiSeOd to in p.ListaLinijskih)
                {
                    poruka = poruka + to.ID_LIN_GOBJ.Naziv + "\n";
                }

                MessageBox.Show("Sastoji se od sledecih linijskih objekata: \n"+poruka);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }
        #endregion

        #region UPDATE
        private void btnUpdateL_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo;
                IList<LinijskiObjekat> listal = s.QueryOver<LinijskiObjekat>().Where(x=>x.Naziv== "E-75").List<LinijskiObjekat>();
                lo = listal[0];

                lo.KLASA_PUTA = "auto-put";
                lo.TIP = "L";

                KoordinateLinijski ko = new KoordinateLinijski();
                ko.ID_GOBJ = lo;
                ko.X=31.5f;
                ko.Y = 45;

                KoordinateLinijski ko1 = new KoordinateLinijski();
                ko1.ID_GOBJ = lo;
                ko1.X = 30;
                ko1.Y = 42.88f;


                lo.ListaKoordinata.Add(ko);
                lo.ListaKoordinata.Add(ko1);
                
                s.SaveOrUpdate(lo);
                s.Flush();

                MessageBox.Show("Promenjen je linijski objekat i dodate su mu nove koordinate x1="+ko.X+",y1="+ko.Y+" i x2="+ko1.X+",y2="+ko1.Y);


                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnUpdateT_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                
                TackastiObjekat to;
                IList<TackastiObjekat> listatac = s.QueryOver<TackastiObjekat>().Where(x=>x.Naziv== "Kursumlija").List<TackastiObjekat>();
                to = listatac[0];

                to.TIP = "T";

                s.SaveOrUpdate(to);
                s.Flush();

                MessageBox.Show("Azuriran je tip. Tackasti: "+to.Naziv+"\nnovi tip: "+to.TIP);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnUpdateP_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();

            PovrsinskiObjekat po; 
            IList<PovrsinskiObjekat> listapov = s.QueryOver<PovrsinskiObjekat>().Where(x=>x.Naziv== "Bubanj").List<PovrsinskiObjekat>();
            po = listapov[0];

            po.NADMORSKA_VISINA = 1422;

            s.SaveOrUpdate(po);
            s.Flush();

            MessageBox.Show("Promenjena je nadmorska visina povrsinskog objekta "+po.Naziv+" i sada iznosi: "+po.NADMORSKA_VISINA);

            s.Close();
        }
        #endregion

        #region DELETE
        private void btnDeleteL_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo;

                IList<LinijskiObjekat> ll = s.QueryOver<LinijskiObjekat>()
                    .Where(x => x.Naziv == "E-75").List<LinijskiObjekat>();

                lo = (LinijskiObjekat)ll[0];


                s.Delete(lo);
                s.Flush();

                MessageBox.Show("Obrisan je linijski objekat sa ID-em: " + lo.ID_GOBJ+", naziv: "+lo.Naziv);
               
                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }

        }

        private void btnDeleteT_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat to;

                IList<TackastiObjekat> tl = s.QueryOver<TackastiObjekat>()
                    .Where(x => x.Naziv == "Kursumlija").List<TackastiObjekat>();

                to = (TackastiObjekat)tl[0];

                s.Delete(to);
                s.Flush();

                MessageBox.Show("Obrisan je tackasti objekat sa ID-em: " + to.ID_GOBJ+", naziv: "+to.Naziv);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void btnDeleteP_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PovrsinskiObjekat po;

                IList<PovrsinskiObjekat> pl = s.QueryOver<PovrsinskiObjekat>()
                   .Where(x => x.Naziv == "Bubanj").List<PovrsinskiObjekat>();

                po = (PovrsinskiObjekat)pl[0];

                foreach (TackastiObjekat t in po.ListaGeografskihObjekata)
                {
                    t.ID_UZVISENJA = null;
                }

                po.ListaGeografskihObjekata = null;

                s.Delete(po);
                s.Flush();

                MessageBox.Show("Obrisan je povrsinski objekat sa ID-em: " + po.ID_GOBJ + ", naziv: " + po.Naziv);

                s.Close();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }
        #endregion

        private void btnSledece_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Sledeca faza stize uskoro!","To be continued..",MessageBoxButtons.OK,MessageBoxIcon.Information);
            // FormApp fa = new FormApp();
            //  fa.Show();

            LogIn forma = new LogIn();
            forma.Show();
        }



        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Za svaki geografski objekat (Linijski, Tackasti, Povrsinski) izvrseno je kreiranje i dodavanje u bazu uz sve dodatne entitete poput Vrha, Simbola i Znamenitosti i na taj nacin je pokrivena svaka provera funkcionisanja veza izmedju entiteta prilikom dodavanja novih podataka u bazu.", "Objasnjenje!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Za svaki geografski objekat (Linijski, Tackasti, Povrsinski) izvrseno je citanje iz baze uz sve dodatne entitete poput Vrha, Simbola i Znamenitosti i na taj nacin je pokrivena svaka provera funkcionisanja veza izmedju entiteta prilikom citanja podataka iz baze.", "Objasnjenje!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Za svaki geografski objekat (Linijski, Tackasti, Povrsinski) izvrseno je azuriranje, tj menjanje vrednosti koje nisu (ili su pogresno) unete prilikom kreiranja podataka i unosa u bazu.", "Objasnjenje!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Za svaki geografski objekat (Linijski, Tackasti, Povrsinski) izvrseno je brisanje podataka iz baze tako da se ne narusi koncept baze i sve reference na taj objekat su takodje izbrisane.", "Objasnjenje!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

}
    

