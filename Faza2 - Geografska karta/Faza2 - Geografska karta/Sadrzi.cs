﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class Sadrzi
    {
        public virtual int ID_SADRZI { get; set; }
        public virtual LinijskiObjekat ID_LIN_GOBJ { get; set; }
        public virtual TackastiObjekat ID_TAC_GOBJ { get; set; }
        public virtual int REDNI_BROJ { get; set; }
        public virtual float RASTOJANJE_DO_PROSLOG { get; set; }
        public virtual float RASTOJANJE_DO_SLEDECEG { get; set; }

        public Sadrzi()
        {

        }
    }
}
