﻿namespace Faza2___Geografska_karta
{
    partial class FormEditDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTip = new System.Windows.Forms.Label();
            this.tbxNaziv = new System.Windows.Forms.TextBox();
            this.lblDuzina = new System.Windows.Forms.Label();
            this.nupDuzina = new System.Windows.Forms.NumericUpDown();
            this.lblKoX = new System.Windows.Forms.Label();
            this.lblKoY = new System.Windows.Forms.Label();
            this.lblEvidencija = new System.Windows.Forms.Label();
            this.nupY = new System.Windows.Forms.NumericUpDown();
            this.nupX = new System.Windows.Forms.NumericUpDown();
            this.dtpEvidencija = new System.Windows.Forms.DateTimePicker();
            this.lblNadmorska = new System.Windows.Forms.Label();
            this.lblTipV = new System.Windows.Forms.Label();
            this.nupNadV = new System.Windows.Forms.NumericUpDown();
            this.cbxTip = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelnaziv = new System.Windows.Forms.Label();
            this.labelnadmorska = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nupDuzina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupNadV)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(12, 237);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Izmeni";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(93, 237);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Obrisi";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Naziv :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tip :";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(148, 44);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(35, 13);
            this.lblTip.TabIndex = 5;
            this.lblTip.Text = "label4";
            // 
            // tbxNaziv
            // 
            this.tbxNaziv.Location = new System.Drawing.Point(151, 17);
            this.tbxNaziv.Name = "tbxNaziv";
            this.tbxNaziv.Size = new System.Drawing.Size(120, 20);
            this.tbxNaziv.TabIndex = 6;
            // 
            // lblDuzina
            // 
            this.lblDuzina.AutoSize = true;
            this.lblDuzina.Location = new System.Drawing.Point(101, 89);
            this.lblDuzina.Name = "lblDuzina";
            this.lblDuzina.Size = new System.Drawing.Size(46, 13);
            this.lblDuzina.TabIndex = 7;
            this.lblDuzina.Text = "Duzina :";
            this.lblDuzina.Visible = false;
            // 
            // nupDuzina
            // 
            this.nupDuzina.Location = new System.Drawing.Point(151, 87);
            this.nupDuzina.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nupDuzina.Name = "nupDuzina";
            this.nupDuzina.Size = new System.Drawing.Size(120, 20);
            this.nupDuzina.TabIndex = 8;
            this.nupDuzina.Visible = false;
            // 
            // lblKoX
            // 
            this.lblKoX.AutoSize = true;
            this.lblKoX.Location = new System.Drawing.Point(73, 89);
            this.lblKoX.Name = "lblKoX";
            this.lblKoX.Size = new System.Drawing.Size(74, 13);
            this.lblKoX.TabIndex = 9;
            this.lblKoX.Text = "Koordinata X :";
            this.lblKoX.Visible = false;
            // 
            // lblKoY
            // 
            this.lblKoY.AutoSize = true;
            this.lblKoY.Location = new System.Drawing.Point(73, 118);
            this.lblKoY.Name = "lblKoY";
            this.lblKoY.Size = new System.Drawing.Size(74, 13);
            this.lblKoY.TabIndex = 10;
            this.lblKoY.Text = "Koordinata Y :";
            this.lblKoY.Visible = false;
            // 
            // lblEvidencija
            // 
            this.lblEvidencija.AutoSize = true;
            this.lblEvidencija.Location = new System.Drawing.Point(52, 145);
            this.lblEvidencija.Name = "lblEvidencija";
            this.lblEvidencija.Size = new System.Drawing.Size(95, 13);
            this.lblEvidencija.TabIndex = 11;
            this.lblEvidencija.Text = "Datum evidencije :";
            this.lblEvidencija.Visible = false;
            // 
            // nupY
            // 
            this.nupY.Location = new System.Drawing.Point(151, 116);
            this.nupY.Name = "nupY";
            this.nupY.Size = new System.Drawing.Size(120, 20);
            this.nupY.TabIndex = 12;
            this.nupY.Visible = false;
            // 
            // nupX
            // 
            this.nupX.Location = new System.Drawing.Point(151, 87);
            this.nupX.Name = "nupX";
            this.nupX.Size = new System.Drawing.Size(120, 20);
            this.nupX.TabIndex = 13;
            this.nupX.Visible = false;
            // 
            // dtpEvidencija
            // 
            this.dtpEvidencija.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEvidencija.Location = new System.Drawing.Point(151, 142);
            this.dtpEvidencija.Name = "dtpEvidencija";
            this.dtpEvidencija.Size = new System.Drawing.Size(120, 20);
            this.dtpEvidencija.TabIndex = 14;
            this.dtpEvidencija.Visible = false;
            // 
            // lblNadmorska
            // 
            this.lblNadmorska.AutoSize = true;
            this.lblNadmorska.Location = new System.Drawing.Point(50, 89);
            this.lblNadmorska.Name = "lblNadmorska";
            this.lblNadmorska.Size = new System.Drawing.Size(97, 13);
            this.lblNadmorska.TabIndex = 15;
            this.lblNadmorska.Text = "Nadmorska visina :";
            this.lblNadmorska.Visible = false;
            // 
            // lblTipV
            // 
            this.lblTipV.AutoSize = true;
            this.lblTipV.Location = new System.Drawing.Point(37, 89);
            this.lblTipV.Name = "lblTipV";
            this.lblTipV.Size = new System.Drawing.Size(110, 13);
            this.lblTipV.TabIndex = 16;
            this.lblTipV.Text = "Tip vodene površine :";
            this.lblTipV.Visible = false;
            // 
            // nupNadV
            // 
            this.nupNadV.Location = new System.Drawing.Point(151, 87);
            this.nupNadV.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            this.nupNadV.Name = "nupNadV";
            this.nupNadV.Size = new System.Drawing.Size(120, 20);
            this.nupNadV.TabIndex = 17;
            this.nupNadV.Visible = false;
            // 
            // cbxTip
            // 
            this.cbxTip.FormattingEnabled = true;
            this.cbxTip.Items.AddRange(new object[] {
            "Jezero",
            "Bara",
            "More"});
            this.cbxTip.Location = new System.Drawing.Point(150, 86);
            this.cbxTip.Name = "cbxTip";
            this.cbxTip.Size = new System.Drawing.Size(121, 21);
            this.cbxTip.TabIndex = 18;
            this.cbxTip.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(233, 237);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Zatvori";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Naziv posvrsinkog objekta :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Nadmorska visina :";
            // 
            // labelnaziv
            // 
            this.labelnaziv.AutoSize = true;
            this.labelnaziv.Location = new System.Drawing.Point(148, 172);
            this.labelnaziv.Name = "labelnaziv";
            this.labelnaziv.Size = new System.Drawing.Size(13, 13);
            this.labelnaziv.TabIndex = 22;
            this.labelnaziv.Text = "lll";
            // 
            // labelnadmorska
            // 
            this.labelnadmorska.AutoSize = true;
            this.labelnadmorska.Location = new System.Drawing.Point(148, 200);
            this.labelnadmorska.Name = "labelnadmorska";
            this.labelnadmorska.Size = new System.Drawing.Size(13, 13);
            this.labelnadmorska.TabIndex = 23;
            this.labelnadmorska.Text = "lll";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(194, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "metara";
            // 
            // FormEditDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 268);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelnadmorska);
            this.Controls.Add(this.labelnaziv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cbxTip);
            this.Controls.Add(this.nupNadV);
            this.Controls.Add(this.lblTipV);
            this.Controls.Add(this.lblNadmorska);
            this.Controls.Add(this.dtpEvidencija);
            this.Controls.Add(this.nupX);
            this.Controls.Add(this.nupY);
            this.Controls.Add(this.lblEvidencija);
            this.Controls.Add(this.lblKoY);
            this.Controls.Add(this.lblKoX);
            this.Controls.Add(this.nupDuzina);
            this.Controls.Add(this.lblDuzina);
            this.Controls.Add(this.tbxNaziv);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Name = "FormEditDelete";
            this.ShowIcon = false;
            this.Text = "Pregled - Izmeni / Obrisi";
            this.Load += new System.EventHandler(this.FormEditDelete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupDuzina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupNadV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTip;
        private System.Windows.Forms.TextBox tbxNaziv;
        private System.Windows.Forms.Label lblDuzina;
        private System.Windows.Forms.NumericUpDown nupDuzina;
        private System.Windows.Forms.Label lblKoX;
        private System.Windows.Forms.Label lblKoY;
        private System.Windows.Forms.Label lblEvidencija;
        private System.Windows.Forms.NumericUpDown nupY;
        private System.Windows.Forms.NumericUpDown nupX;
        private System.Windows.Forms.DateTimePicker dtpEvidencija;
        private System.Windows.Forms.Label lblNadmorska;
        private System.Windows.Forms.Label lblTipV;
        private System.Windows.Forms.NumericUpDown nupNadV;
        private System.Windows.Forms.ComboBox cbxTip;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelnaziv;
        private System.Windows.Forms.Label labelnadmorska;
        private System.Windows.Forms.Label label7;
    }
}