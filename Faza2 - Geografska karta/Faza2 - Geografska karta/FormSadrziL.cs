﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Faza2___Geografska_karta
{
    public partial class FormSadrziL : Form
    {

        public int rbr { get; set; }
        public int rastojanjeSl { get; set; }
        public int rastojanjePro { get; set; }


        public FormSadrziL()
        {
            InitializeComponent();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.rbr =(int) nupRB.Value;
            this.rastojanjeSl = (int)nupSl.Value;
            this.rastojanjePro = (int)nupPro.Value;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
