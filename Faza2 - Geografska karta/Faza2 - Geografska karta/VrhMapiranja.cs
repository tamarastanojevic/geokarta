﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class VrhMapiranja : ClassMap<Vrh>
    {
        public VrhMapiranja()
        {
            Table("VRH");

            Id(x => x.ID_VRH, "ID_VRH").GeneratedBy.TriggerIdentity();

            Map(x => x.IME).Column("IME");
            Map(x => x.VISINA).Column("VISINA").Nullable();

    //        References(x => x.ID_UZVISENJA).Column("ID_UZVISENJA").LazyLoad();
        }
    }
}
