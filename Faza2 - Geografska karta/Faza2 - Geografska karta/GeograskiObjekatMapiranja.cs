﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class GeograskiObjekatMapiranja : ClassMap<GeografskiObjekat>
    {
        public GeograskiObjekatMapiranja()
        {
            Table("GEOGRAFSKI_OBJEKAT");

            Id(x => x.ID_GOBJ, "ID_GOBJ").GeneratedBy.TriggerIdentity();

            Map(x => x.Naziv).Column("NAZIV");
            Map(x => x.Nadmorska_visina).Column("NADMORSKA_VISINA").Nullable();
            Map(x => x.TIP).Column("TIP").Nullable();

   //         References(x => x.ID_UZVISENJA).Column("ID_UZVISENJA").LazyLoad();
        }
    }
}
