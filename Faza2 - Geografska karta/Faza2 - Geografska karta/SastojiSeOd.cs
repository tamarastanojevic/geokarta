﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class SastojiSeOd
    {
        public virtual int ID_SASTOJI { get; set; }
        public virtual PovrsinskiObjekat ID_POV_GOBJ { get; set; }
        public virtual LinijskiObjekat ID_LIN_GOBJ { get; set; }

        public SastojiSeOd()
        {
            
        }
    }
}
