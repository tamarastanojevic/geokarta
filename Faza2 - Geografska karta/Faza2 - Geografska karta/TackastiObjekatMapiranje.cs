﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class TackastiObjekatMapiranje : SubclassMap<TackastiObjekat>
    {
        public TackastiObjekatMapiranje()
        {
            Table("TACKASTI_OBJEKAT");

            KeyColumn("ID_GOBJ");
            
            Map(x => x.KOORD_X).Column("KOORD_X");
            Map(x => x.KOORD_Y).Column("KOORD_Y");
            Map(x => x.DATUM_EVIDENCIJE).Column("DATUM_EVIDENCIJE");
            Map(x => x.NASELJENO_FLEG).Column("NASELJENO_FLEG");
            Map(x => x.OPSTINA).Column("OPSTINA");
            Map(x => x.BROJ_STANOVNIKA).Column("BROJ_STANOVNIKA");
            Map(x => x.DATUM_OSNIVANJA).Column("DATUM_OSNIVANJA");
            Map(x => x.TURISTICKO_FLEG).Column("TURISTICKO_FLEG");
            Map(x => x.DATUM_TUR).Column("DATUM_TUR");
            Map(x => x.TIP_TURISTICKOG).Column("TIP");

    //        References(x => x.ID_SIMBOLA).Column("ID_SIMBOLA").LazyLoad();

      //      HasMany(x => x.ListaZnamenitosti).KeyColumn("ID_NASELJENO").LazyLoad().Cascade.All().Inverse();
      //      HasMany(x => x.ListaLinijskih).KeyColumn("ID_TAC_GOBJ").LazyLoad().Cascade.All().Inverse();
        }
    }
}
