﻿namespace Faza2___Geografska_karta
{
    partial class FormSadrziL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.nupSl = new System.Windows.Forms.NumericUpDown();
            this.nupPro = new System.Windows.Forms.NumericUpDown();
            this.nupRB = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nupSl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupPro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupRB)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Redni broj tačkastog :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Rastojanje do sledećeg :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rastojanje do prošlog :";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(116, 121);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnZatvori
            // 
            this.btnZatvori.Location = new System.Drawing.Point(197, 121);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(75, 23);
            this.btnZatvori.TabIndex = 7;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // nupSl
            // 
            this.nupSl.Location = new System.Drawing.Point(152, 78);
            this.nupSl.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nupSl.Name = "nupSl";
            this.nupSl.Size = new System.Drawing.Size(120, 20);
            this.nupSl.TabIndex = 5;
            // 
            // nupPro
            // 
            this.nupPro.Location = new System.Drawing.Point(152, 52);
            this.nupPro.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nupPro.Name = "nupPro";
            this.nupPro.Size = new System.Drawing.Size(120, 20);
            this.nupPro.TabIndex = 3;
            // 
            // nupRB
            // 
            this.nupRB.Location = new System.Drawing.Point(152, 22);
            this.nupRB.Name = "nupRB";
            this.nupRB.Size = new System.Drawing.Size(120, 20);
            this.nupRB.TabIndex = 1;
            // 
            // FormSadrziL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 157);
            this.Controls.Add(this.nupRB);
            this.Controls.Add(this.nupPro);
            this.Controls.Add(this.nupSl);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormSadrziL";
            this.ShowIcon = false;
            this.Text = "Veza Sadrzi";
            ((System.ComponentModel.ISupportInitialize)(this.nupSl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupPro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupRB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.NumericUpDown nupSl;
        private System.Windows.Forms.NumericUpDown nupPro;
        private System.Windows.Forms.NumericUpDown nupRB;
    }
}