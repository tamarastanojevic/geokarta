﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class KoordinateLinijski
    {
        public virtual int ID_KOORD { get; set; }
        public virtual LinijskiObjekat ID_GOBJ { get; set; }
        public virtual float X { get; set; }
        public virtual float Y { get; set; }
    }
}
