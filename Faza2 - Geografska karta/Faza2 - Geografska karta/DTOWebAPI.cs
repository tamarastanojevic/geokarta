﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Faza2___Geografska_karta.Views;

namespace Faza2___Geografska_karta
{
    public class DTOWebAPI
    {
        public IEnumerable<TackastiObjekat> GetTackasti()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<TackastiObjekat> eto = from x in s.Query<TackastiObjekat>()
                                               where x.NASELJENO_FLEG == true
                                               select x;

            return eto;
        }

        public IEnumerable<LinijskiObjekat> GetLinijski()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<LinijskiObjekat> elo = from x in s.Query<LinijskiObjekat>()
                                               where x.DUZINA > 500
                                               select x;

            return elo;
        }

        public IEnumerable<PovrsinskiObjekat> GetPovrsinski()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<PovrsinskiObjekat> epo = from x in s.Query<PovrsinskiObjekat>()
                                                 where x.VODENE_POVRSINE_FLEG == true
                                                 select x;

            return epo;
        }

        public IEnumerable<Znamenitost> GetZnamenitosti()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Znamenitost> ez = from x in s.Query<Znamenitost>() select x;

            return ez;
        }

        public IEnumerable<Simbol> GetSimboli()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Simbol> es = from x in s.Query<Simbol>() select x;

            return es;
        }

        public IEnumerable<Vrh> GetVrhovi()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Vrh> ev = from x in s.Query<Vrh>() select x;

            return ev;
        }

        public TackastiView GetTajTackasti(int y)
        {
            ISession s = DataLayer.GetSession();

            TackastiObjekat tob = s.Query<TackastiObjekat>().Where(tt => tt.ID_GOBJ == y).Select(p => p).FirstOrDefault();

            if (tob == null) return new TackastiView();

            return new TackastiView(tob);
        }

        public PovrsinskiView GetTajPovrsinski(int y)
        {
            ISession s = DataLayer.GetSession();

            PovrsinskiObjekat pob = s.Query<PovrsinskiObjekat>().Where(p => p.ID_GOBJ == y)
                            .Select(p => p).FirstOrDefault();
            
            if (pob == null) return new PovrsinskiView();

            return new PovrsinskiView(pob);
        }

        public LinijskiView GetTajLinijski(int y)
        {
            ISession s = DataLayer.GetSession();

            LinijskiObjekat lob = s.Query<LinijskiObjekat>().Where(ll=> ll.ID_GOBJ == y).Select(p => p).FirstOrDefault();

            if (lob == null) return new LinijskiView();

            return new LinijskiView(lob);
        }

        public ZnamenitostView GetTajZnaminotost(int y)
        {
            ISession s = DataLayer.GetSession();

            Znamenitost z = s.Query<Znamenitost>().Where(zn => zn.ID_ZNAMENITOST== y).Select(p => p).FirstOrDefault();

            if (z == null) return new ZnamenitostView();

            return new ZnamenitostView(z);
        }

        public SimbolView GetTajSimbol(int y)
        {
            ISession s = DataLayer.GetSession();

            Simbol sim = s.Query<Simbol>().Where(simm => simm.ID_SIMBOL == y).Select(p => p).FirstOrDefault();

            if (sim == null) return new SimbolView();

            return new SimbolView(sim);
        }

        public VrhView GetTajVrh(int y)
        {
            ISession s = DataLayer.GetSession();

            Vrh v = s.Query<Vrh>().Where(vrh => vrh.ID_VRH == y).Select(p => p).FirstOrDefault();

            if (v == null) return new VrhView();

            return new VrhView(v);

        }

        public int DodajPovrsinski(PovrsinskiObjekat p)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(p);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int DodajTackasti(TackastiObjekat t)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(t);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int DodajLinijski(LinijskiObjekat l)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(l);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int DodajZnamenitost(Znamenitost z)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int DodajSimbol(Simbol sim)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(sim);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int DodajVrh(Vrh v)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(v);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }
        }

        public int PromeniTackasti(TackastiObjekat to)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(to);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int PromeniLinijski(LinijskiObjekat lo)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(lo);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int PromeniPovrsinski(PovrsinskiObjekat po)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(po);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int PromeniZnamenitost(Znamenitost z)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(z);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int PromeniSimbol(Simbol sim)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(sim);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int PromeniVrh(Vrh v)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                s.SaveOrUpdate(v);
                s.Flush();

                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int BrisiTackasti(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                TackastiObjekat to = s.Load<TackastiObjekat>(a);

                s.Delete(to);

                s.Flush();
                s.Close();

                return 1;
            }
            catch(Exception ex)
            {
                return -1;
            }
        }

        public int BrisiPovrsinski(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                PovrsinskiObjekat po = s.Load<PovrsinskiObjekat>(a);

                s.Delete(po);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int BrisiLinijski(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                LinijskiObjekat lo = s.Load<LinijskiObjekat>(a);

                s.Delete(lo);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int BrisiZnamenitost(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Znamenitost z = s.Load<Znamenitost>(a);

                s.Delete(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int BrisiSimbol(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Simbol z = s.Load<Simbol>(a);

                s.Delete(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int BrisiVrh(int a)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Vrh z = s.Load<Vrh>(a);

                s.Delete(z);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

    }
}
