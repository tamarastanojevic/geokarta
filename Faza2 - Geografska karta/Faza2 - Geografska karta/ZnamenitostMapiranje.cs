﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class ZnamenitostMapiranje : ClassMap<Znamenitost>
    {
        public ZnamenitostMapiranje()
        {
            Table("ZNAMENITOST");

            Id(x => x.ID_ZNAMENITOST, "ID_ZNAMENITOST").GeneratedBy.TriggerIdentity();

            Map(x => x.NAZIV).Column("NAZIV");

  //          References(x => x.ID_NASELJENO).Column("ID_NASELJENO").LazyLoad();
        }
    }
}
