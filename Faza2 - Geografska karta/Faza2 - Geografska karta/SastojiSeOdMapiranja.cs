﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta
{
    public class SastojiSeOdMapiranja : ClassMap<SastojiSeOd>
    {
        public SastojiSeOdMapiranja()
        {
            Table("SASTOJI_SE_OD");

            Id(x => x.ID_SASTOJI, "ID_SASTOJI").GeneratedBy.TriggerIdentity();

            References(x => x.ID_LIN_GOBJ).Column("ID_LIN_GOBJ").LazyLoad();
            References(x => x.ID_POV_GOBJ).Column("ID_POV_GOBJ").LazyLoad();
        }
    }
}
