﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class TackastiView
    {
        public double KOORD_X { get; set; }
        public double KOORD_Y { get; set; }
        public DateTime DATUM_EVIDENCIJE { get; set; }
        public bool NASELJENO_FLEG { get; set; }
        public string OPSTINA { get; set; }
        public int BROJ_STANOVNIKA { get; set; }
        public DateTime DATUM_OSNIVANJA { get; set; }
        public bool TURISTICKO_FLEG { get; set; }
        public DateTime DATUM_TUR { get; set; }
        public string TIP_TURISTICKOG { get; set; }
        public Simbol ID_SIMBOLA { get; set; }

        public IList<Znamenitost> ListaZnamenitosti { get; set; }
        public IList<Sadrzi> ListaLinijskih { get; set; }

        public TackastiView(TackastiObjekat t)
        {
            this.KOORD_X = t.KOORD_X;
            this.KOORD_Y = t.KOORD_Y;
            this.DATUM_EVIDENCIJE = t.DATUM_EVIDENCIJE;
            this.NASELJENO_FLEG = t.NASELJENO_FLEG;
            this.OPSTINA = t.OPSTINA;
            this.BROJ_STANOVNIKA = t.BROJ_STANOVNIKA;
            this.DATUM_OSNIVANJA = t.DATUM_OSNIVANJA;
            this.TURISTICKO_FLEG = t.TURISTICKO_FLEG;
            this.DATUM_TUR = t.DATUM_TUR;
            this.TIP_TURISTICKOG = t.TIP_TURISTICKOG;
            this.ID_SIMBOLA = t.ID_SIMBOLA;
        }

        public TackastiView()
        {

        }

    }
}
