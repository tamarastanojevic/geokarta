﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class LinijskiView
    {
        public int DUZINA { get; set; }
        public bool REKA_FLEG { get; set; }
        public bool PUT_FLEG { get; set; }
        public string KLASA_PUTA { get; set; }
        public bool GRANICNA_LINIJA_FLEG { get; set; }
        public string IME_DRZAVE { get; set; }

        public IList<SastojiSeOd> ListaPovrsinskih { get; set; }
        public IList<Sadrzi> ListaTackastih { get; set; }
        public IList<KoordinateLinijski> ListaKoordinata { get; set; }

        public LinijskiView(LinijskiObjekat l)
        {
            this.ListaKoordinata = l.ListaKoordinata;
            this.ListaPovrsinskih = l.ListaPovrsinskih;
            this.ListaTackastih = l.ListaTackastih;

            this.DUZINA = l.DUZINA;
            this.REKA_FLEG = l.REKA_FLEG;
            this.PUT_FLEG = l.PUT_FLEG;
            this.KLASA_PUTA = l.KLASA_PUTA;
            this.GRANICNA_LINIJA_FLEG = l.GRANICNA_LINIJA_FLEG;
            this.IME_DRZAVE = l.IME_DRZAVE;
        }

        public LinijskiView()
        {

        }
    }
}
