﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class ZnamenitostView
    {
        public int ID_ZNAMENITOST { get; set; }
        public string NAZIV { get; set; }
        public TackastiObjekat ID_NASELJENO { get; set; }

        public ZnamenitostView(Znamenitost z)
        {
            this.ID_ZNAMENITOST = z.ID_ZNAMENITOST;
            this.NAZIV = z.NAZIV;
            this.ID_NASELJENO = z.ID_NASELJENO;
        }

        public ZnamenitostView()
        {

        }
    }
}
