﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faza2___Geografska_karta.Views
{
    public class SimbolView
    {
        public int ID_SIMBOL { get; set; }
        public int OD_BROJ_STANOVNIKA { get; set; }
        public int DO_BROJ_STANOVNIKA { get; set; }
        public string NAZIV { get; set; }


        public SimbolView(Simbol s)
        {
            this.ID_SIMBOL = s.ID_SIMBOL;
            this.OD_BROJ_STANOVNIKA = s.OD_BROJ_STANOVNIKA;
            this.DO_BROJ_STANOVNIKA = s.DO_BROJ_STANOVNIKA;
            this.NAZIV = s.NAZIV;
        }

        public SimbolView()
        {

        }
    }
}
