﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class ZnamenitostController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/znamenitost/")]
        public IEnumerable<Znamenitost> Get()
        {
            DTOWebAPI webi = new DTOWebAPI();

            IEnumerable<Znamenitost> ze = webi.GetZnamenitosti();

            return ze;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/znamenitost/{x}")]
        public ZnamenitostView Get(int x)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.GetTajZnaminotost(x);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/znamenitost/dodaj/")]
        public int Post([FromBody] Znamenitost z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.DodajZnamenitost(z);
        }
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/znamenitost/izmeni/{id}")]
        public int Put(int id, [FromBody]Znamenitost z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            z.ID_ZNAMENITOST = id;

            return webi.PromeniZnamenitost(z);
        }
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/znamenitost/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.BrisiZnamenitost(id);
        }
    }
}
