﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Faza2___Geografska_karta;
using Faza2___Geografska_karta.Views;

namespace WebGeoKarta.Controllers
{
    public class VrhController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/vrh/")]
        public IEnumerable<Vrh> Get()
        {
            DTOWebAPI webi = new DTOWebAPI();

            IEnumerable<Vrh> ze = webi.GetVrhovi();

            return ze;
        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/vrh/{x}")]
        public VrhView Get(int x)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.GetTajVrh(x);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/vrh/dodaj/")]
        public int Post([FromBody] Vrh z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.DodajVrh(z);

        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/vrh/izmeni/{id}")]
        public int Put(int id, [FromBody]Vrh z)
        {
            DTOWebAPI webi = new DTOWebAPI();

            z.ID_VRH = id;

            return webi.PromeniVrh(z);
        }
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/vrh/obrisi/{id}")]
        public int Delete(int id)
        {
            DTOWebAPI webi = new DTOWebAPI();

            return webi.BrisiVrh(id);
        }
    }
}
